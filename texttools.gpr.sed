--    Build project for Textools.
--    Copyright (C) 2009-2012 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

project Texttools is
  Soversion := "SOVERSION";
  Ldlibs := ("LDLIBS");

  type Library_Kind is ("dynamic", "static");
  Kind : Library_Kind := External ("KIND");

  for Library_Name use Project'Name;
  case Kind is
     when "dynamic" =>
        for Library_Version use "lib" & Project'Library_Name & ".so." & Soversion;
        --  Build-time options use to link the shared library.
        for Library_Options use ("LDFLAGS") & Ldlibs;
     when "static" =>
        null;
  end case;
  for Library_Kind use Kind;
  for Library_Dir use "build-lib-" & Kind;
  for Object_Dir use "build-obj-" & Kind;
  package Linker is
     --  This package only exists to pass more options to importing
     --  projects like demo/demo.gpr.
     for Linker_Options use Ldlibs;
  end Linker;
  package Compiler is
     for Default_Switches ("Ada") use ("ADAFLAGS");
     for Default_Switches ("C") use ("CFLAGS");
  end Compiler;
  for Languages use ("Ada", "C");
  for Source_Dirs use ("src");
end Texttools;
