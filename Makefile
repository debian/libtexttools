# Build script for Texttools.
# Copyright (c) 2003-2009 Ludovic Brenta <lbrenta@debian.org>
# Copyright (c) 2009-2012 Nicolas Boulenguez <nicolas.boulenguez@free.fr>

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################
# Global configuration #
########################

LIB_NAME := texttools

SOVERSION := 1

################################
# Build and test configuration #
################################

# Use environment variables if available, this is common practice for
# CPPFLAGS, CFLAGS and LDFLAGS.
CFLAGS   ?= -O2
ADAFLAGS ?= -O2

.NOTPARALLEL:
GNATMAKE_OPTIONS := -j$(shell getconf _NPROCESSORS_ONLN)

ifdef ALL_CHECKS
  CFLAGS += -Wall -Wextra -Wformat -Wformat-security -g
  ADAFLAGS += -Wall -Wextra -g -gnatE -gnatQ -gnatVa -gnata -gnatf	\
  -gnato -gnatq -gnatySdx -gnaty0 -gnatyM159 -gnatw.e -gnatwH
  GNATMAKE_OPTIONS += -s -we
endif

# We want the same compiler for C and Ada sources.
CC := gnatgcc

LDLIBS := $(shell ncurses5-config --libs)

##############################
# Installation configuration #
##############################

# Each of the following path should be prefixed with
DESTDIR :=

# The sources files are installed into a LIB_NAME subdirectory of
SRC_DIR := usr/share/ada/adainclude

# A LIB_NAME.gpr project convenient for library usage is installed into
GPR_DIR := usr/share/ada/adainclude
# The content of this file ignores DESTDIR.

# The GNAT ALI files are installed into a LIB_NAME subdirectory of
ALI_DIR := usr/lib/ada/adalib

# The static and dynamic library are installed into
LIB_DIR := usr/lib

#########
# Rules #
#########

build: build-dynamic build-static

build-dynamic build-static: build-%: $(LIB_NAME).gpr
	gnatmake -P$< $(GNATMAKE_OPTIONS) -XKIND=$*
clean::
	rm -f $(foreach dir,obj lib,$(foreach kind,dynamic static,build-$(dir)-$(kind)/*))

clean::
	find -name "*~" -delete

test: examples/examples.gpr build-static
	gnatmake -P$< $(GNATMAKE_OPTIONS) -XKIND=static
# Texttools.gpr is found in the current directory when executing this
# recipe, and will be found in the default system location after
# installation.

clean:: examples/examples.gpr texttools.gpr
	gnatclean -P$< -XKIND=static
	rm -f $^
# We need to create them for gnatclean, then suppress it as the last action.

install: build
	install --directory $(DESTDIR)/$(SRC_DIR)/$(LIB_NAME)
	install --mode=644 src/*.ad[sb] src/*.[ch] $(DESTDIR)/$(SRC_DIR)/$(LIB_NAME)
	install --directory $(DESTDIR)/$(GPR_DIR)
	sed template_for_installed_project \
          $(foreach var,LIB_NAME SRC_DIR ALI_DIR LDLIBS LIB_DIR, \
          -e 's/$$($(var))/$(subst $(space),"$(comma) ",$($(var)))/g') \
          > $(DESTDIR)/$(GPR_DIR)/$(LIB_NAME).gpr
	chmod 644 $(DESTDIR)/$(GPR_DIR)/$(LIB_NAME).gpr
	install --directory $(DESTDIR)/$(ALI_DIR)/$(LIB_NAME)
	install --mode=444 build-lib-dynamic/*.ali $(DESTDIR)/$(ALI_DIR)/$(LIB_NAME)
	install --directory $(DESTDIR)/$(LIB_DIR)
	install --mode=644 build-lib-static/lib$(LIB_NAME).a $(DESTDIR)/$(LIB_DIR)
	install --mode=644 build-lib-dynamic/lib$(LIB_NAME).so.$(SOVERSION) $(DESTDIR)/$(LIB_DIR)
	cd $(DESTDIR)/$(LIB_DIR) && ln --force --symbolic lib$(LIB_NAME).so.$(SOVERSION) lib$(LIB_NAME).so

uninstall:
	rm -rf $(DESTDIR)/$(SRC_DIR)/$(LIB_NAME)
	rm -f $(DESTDIR)/$(GPR_DIR)/$(LIB_NAME).gpr
	rm -rf $(DESTDIR)/$(ALI_DIR)/$(LIB_NAME)
	rm -f $(DESTDIR)/$(LIB_DIR)/lib$(LIB_NAME).a
	rm -f $(DESTDIR)/$(LIB_DIR)/lib$(LIB_NAME).so.$(SOVERSION)
	rm -f $(DESTDIR)/$(LIB_DIR)/lib$(LIB_NAME).so

############################################################
#  All that C stuff will be unnecessary with gprbuild’s mixed C/Ada
#  project files.  For the moment, gnatmake will embed all .o files,
#  we only have to compile them and store them in the object dir.

C_SRC := $(wildcard src/*.c)

C_OBJ_DYNAMIC := $(patsubst src/%.c,build-obj-dynamic/%.o,$(C_SRC))
build-dynamic: $(C_OBJ_DYNAMIC)
$(C_OBJ_DYNAMIC): build-obj-dynamic/%.o: src/%.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ -fPIC

C_OBJ_STATIC  := $(patsubst src/%.c,build-obj-static/%.o, $(C_SRC))
build-static: $(C_OBJ_STATIC)
$(C_OBJ_STATIC): build-obj-static/%.o: src/%.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

C_OBJ_TEST    := $(patsubst src/%.c,examples/%.o,  $(C_SRC))
test: $(C_OBJ_TEST)
$(C_OBJ_TEST): CFLAGS += -g -Wall -Wextra
$(C_OBJ_TEST): examples/%.o: src/%.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@
clean::
	rm -f $(C_OBJ_TEST)

# gnatmake 4.4 does not handle External_As_List, so we emulate it with
# a template instead of passing the options with -X.
comma := ,
empty :=
space := $(empty) $(empty)
texttools.gpr examples/examples.gpr: %.gpr: %.gpr.sed
	sed $< \
	 $(foreach var,ADAFLAGS CFLAGS LDFLAGS LDLIBS SOVERSION, \
	   -e 's/$(var)/$(subst $(space),"$(comma) ",$($(var)))/') \
	 > $@

.PHONY: build build-dynamic build-static clean install test uninstall
