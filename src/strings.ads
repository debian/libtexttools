------------------------------------------------------------------------------
-- STRINGS                                                                  --
--                                                                          --
-- Part of TextTools                                                        --
-- Designed and Programmed by Ken O. Burtch                                 --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--                 Copyright (C) 1999-2007 Ken O. Burtch                    --
--                                                                          --
-- This is free software;  you can  redistribute it  and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 2,  or (at your option) any later ver- --
-- sion.  This is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License --
-- for  more details.  You should have  received  a copy of the GNU General --
-- Public License  distributed with this;  see file COPYING.  If not, write --
-- to  the Free Software Foundation,  59 Temple Place - Suite 330,  Boston, --
-- MA 02111-1307, USA.                                                      --
--                                                                          --
-- As a special exception,  if other files  instantiate  generics from this --
-- unit, or you link  this unit with other files  to produce an executable, --
-- this  unit  does not  by itself cause  the resulting  executable  to  be --
-- covered  by the  GNU  General  Public  License.  This exception does not --
-- however invalidate  any other reasons why  the executable file  might be --
-- covered by the  GNU Public License.                                      --
--                                                                          --
-- This is maintained at http://www.pegasoft.ca/tt.html                     --
--                                                                          --
------------------------------------------------------------------------------
with common; use common;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
pragma Elaborate( common ); -- remind Ada the common elaborates first
  
package strings is

---> Misc Functions
--
-- FixSpacing - remove leading/trailing spaces, etc.
-- PhoneticsOf - compute English phonetics of the string
-- TypoOf - true if first string is a typo of the second
-- Tokenize - represent the position of the string in a list as an
--            encoded character, or ' ' if not in list or list too long
-- Untokenize - return the string represented by the encoded character
-- FGREP - search for a string is a list of text

procedure FixSpacing( s : in out Unbounded_String );
function PhoneticsOf( s : String ) return string;
function TypoOf( BadString, GoodString : String ) return boolean;
procedure Tokenize( s : in string; words : in out StrList.Vector;
   ch : in out character );
procedure Untokenize( ch : character ; words : in out StrList.Vector;
  s : in out Unbounded_String );

function FGREP (s : in String;
		text : in String;
		filter_out : boolean := false;
		Case_Insensitive : Boolean := False)
	       return boolean;
-- implementation of UNIX fgrep for a single line of text
-- true if fgrep matches

function FGREP (s : in String;
		text : in String;
		filter_out : in boolean := false;
		case_insensitive : in boolean := false )
	       return string;
-- implementation of UNIX fgrep for a single line of text
-- returns the line if grep matches

procedure FGREP (s : in String;
		 text : in out StrList.Vector;
		 filter_out : boolean := false;
		 case_insensitive : boolean := false );
-- implementation of UNIX fgrep for a list of strings
-- filters in/out matching strings

procedure FGREP (s : in String;
		 text : in StrList.Vector;
		 result : out boolean;
		 filter_out : boolean := false;
		 case_insensitive : boolean := false );
   -- implementation of UNIX fgrep for a list of strings
   -- result is true if there were any matches


---> ASCII Encoding/Decoding
--
-- Compresses and appends a basic data item to the given string

subtype EncodedString is Unbounded_String;

procedure Encode( estr : in out EncodedString; b : in boolean );
procedure Encode( estr : in out EncodedString; c : in character );
procedure Encode( estr : in out EncodedString; i : in integer );
procedure Encode( estr : in out EncodedString; l : in Long_Integer );
procedure Encode( estr : in out EncodedString; r : in ARect );
procedure Encode( estr : in out EncodedString; s : in String );

procedure Decode( estr : in out EncodedString; b : out boolean );
procedure Decode( estr : in out EncodedString; c : out character );
procedure Decode( estr : in out EncodedString; i : out integer );
procedure Decode( estr : in out EncodedString; l : out Long_Integer );
procedure Decode( estr : in out EncodedString; r : out ARect );
procedure Decode( estr : in out EncodedString; s : out Unbounded_String );

type packed_string is new string;


--  BASIC PACK
--
-- Compress string s using dipthong compression resulting in a new string of
-- 50% to 100% the size of the original.  s must contain only lower ASCII
-- characters since the upper ASCII characters are used for the compression.
------------------------------------------------------------------------------

function basic_pack( s : string ) return packed_string;


--  UNPACK
--
-- Decompress string s that was compressed using basic_pack.
------------------------------------------------------------------------------

function unpack( s : packed_string ) return string;


end strings;

