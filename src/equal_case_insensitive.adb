with Ada.Characters.Handling;

function Equal_Case_Insensitive (Left, Right : String) return Boolean is
   use Ada.Characters.Handling;
begin
   if Left'Length /= Right'Length then
      return False;
   end if;
   for I in 0 .. Left'Length-1 loop
      if To_Upper (Left (Left'First + I)) /= To_Upper (Right (Right'First + I)) then
	 return False;
      end if;
   end loop;
   return True;
end Equal_Case_Insensitive;
