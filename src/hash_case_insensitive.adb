with Ada.Strings.Fixed.Hash;
with Ada.Strings.Maps.Constants;

function Hash_Case_Insensitive
  (Key : String)
  return Ada.Containers.Hash_Type
is
   use Ada.Strings;
begin
   return Fixed.Hash (Fixed.Translate (Key, Maps.Constants.Upper_Case_Map));
end Hash_Case_Insensitive;
