with Ada.Containers;

function Hash_Case_Insensitive
  (Key : String)
  return Ada.Containers.Hash_Type;
