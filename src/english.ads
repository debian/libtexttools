------------------------------------------------------------------------------
-- ENGLISH                                                                  --
--                                                                          --
-- Part of TextTools                                                        --
-- Designed and Programmed by Ken O. Burtch                                 --
--                                                                          --
------------------------------------------------------------------------------
--                                                                          --
--                 Copyright (C) 1999-2007 Ken O. Burtch                    --
--                                                                          --
-- This is free software;  you can  redistribute it  and/or modify it under --
-- terms of the  GNU General Public License as published  by the Free Soft- --
-- ware  Foundation;  either version 2,  or (at your option) any later ver- --
-- sion.  This is distributed in the hope that it will be useful, but WITH- --
-- OUT ANY WARRANTY;  without even the  implied warranty of MERCHANTABILITY --
-- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License --
-- for  more details.  You should have  received  a copy of the GNU General --
-- Public License  distributed with this;  see file COPYING.  If not, write --
-- to  the Free Software Foundation,  59 Temple Place - Suite 330,  Boston, --
-- MA 02111-1307, USA.                                                      --
--                                                                          --
-- As a special exception,  if other files  instantiate  generics from this --
-- unit, or you link  this unit with other files  to produce an executable, --
-- this  unit  does not  by itself cause  the resulting  executable  to  be --
-- covered  by the  GNU  General  Public  License.  This exception does not --
-- however invalidate  any other reasons why  the executable file  might be --
-- covered by the  GNU Public License.                                      --
--                                                                          --
-- This is maintained at http://www.pegasoft.ca/tt.html                     --
--                                                                          --
------------------------------------------------------------------------------
package English is

  -- Name of this language for being displayed in "About" window
  -- should be in the form "language translation".  eg. "English
  -- Translation"

  s_languagepackage : constant String := "English Translation";

  -- Buttons
  --
  -- This is the text for screen buttons and menu items.
  -- The "Hot" character is the hilighted letter in the text that
  -- you press to activate the button (usually first letter).

  s_About      : constant String := "About";
  s_About_Hot  : constant character := 'a';
  s_Cancel     : constant String := "Cancel";
  s_Cancel_Hot : constant character := 'l';
  -- uses l instead of c so not to conflict with close
  s_Close      : constant String := "Close";
  s_Close_Hot  : constant character := 'c';
  s_Find       : constant String := "Find";
  s_Find_Hot   : constant character := 'f';
  s_Next       : constant String := "Next";
  s_Next_Hot   : constant character := 'n';
  s_No         : constant String := "No";
  s_No_Hot     : constant character := 'n';
  s_OK         : constant String := "OK";
  s_OK_Hot     : constant character := 'o';
  s_Print      : constant String := "Print";
  s_Print_Hot  : constant character := 'p';
  s_Save       : constant String := "Save";
  s_Save_Hot   : constant character := 's';
  s_Yes        : constant String := "Yes";
  s_Yes_Hot    : constant character := 'y';

  -- Accessories Menu

  s_Cal        : constant String := "Calendar";
  s_Cal_Hot    : constant character := 'c';
  s_CalTitle   : constant String := "Calendar for ";
                 -- eg. "Calendar for 1998"

  -- Common Window Titles

  s_Note       : constant String := "Note";
  s_Caution    : constant String := "Caution";
  s_Warning    : constant String := "Warning";

  -- Other Common Words

  s_Working  : constant String := "Working";

end English;
