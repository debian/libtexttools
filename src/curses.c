/****************************************************
 *                                                  *
 * Ada-to-C interface for Curses library            *
 *                                                  *
 * Compile: gcc -O -c curses.c                      *
 * Bind:    include C_code/curses.o -lcurses        *
 *                                                  *
 ****************************************************/

/* Mouse support incomplete: mouse causes character gets to be non-
   blocking...was going to try GetEvent, but since GPM is GNU,
   there was no point */

/* Configuration ---------------------------------------------- */

/* #define NCURSES3 */      /* define for NCURSES 3.x */
#define NCURSES5            /* define for NCURSES 5.x */
#define NCURSES       /* define for Linux NCURSES library */
                      /* curses is freeware */
/* #define GPM */     /* define for Linux GPM library */
                      /* unfortunately GPM is under GNU licence */

/* Includes ----------------------------------------------------*/

#include <sys/types.h>  /* first 3 just to get O_WRONLY */
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#ifdef GPM
#include <gpm.h>
#endif
#ifdef NCURSES
/* #include <ncurses/ncurses.h> */
/* #include <curses.h> */
#include <curses.h>
#include <stdio.h>
#else
#include <curses.h>
#endif
#include <string.h>

extern char Interface_String[255];

/* Curses Globals ------------------------------------------------- */

int colour_flag; /* true if has_colours() is true, shared with Ada */
int lines, cols; /* screen dimensions, shared with Ada */
chtype chline[181]; /* line of characters -- shouldn't be more than 133 */
char   sline[181];


/* Mouse Globals -------------------------------------------------- */

#ifdef GPM
Gpm_Connect conn;  /* General Purpose Mouse Connection */
int mouse_fid;     /* File ID for the mouse */
#endif

MEVENT mousedata;  /* ncurses mouse data */
int C_mousex;      /* mouse x coordinate, shared with Ada */
int C_mousey;      /* mouse y coordinate, shared with Ada */
int C_hasmouse;    /* 1 = mouse is running, shared with Ada */
/* for GPM */
int C_mousebutton; /* 1 = mouse button down, shared with Ada */
                   /* Ada will set to -1 when handled */


/* Mouse Handler - GPM Prototype */

#ifdef GPM
int MouseHandler(Gpm_Event * event, void *data);
#endif

void CMoveTo( x, y )
  int x, y;
{
  move( y, x );
}

char CGetChar( x, y )
  int x, y;
{
  return mvinch( y, x );
}

long CGetXY()
{ int x, y;
  getyx( stdscr, y, x );
  return y*256+x;
}

void CTextStyle( char bold, char so, char under )
{
#ifdef NCURSES3
  int current;
#else
  attr_t current = 0;
  short temp = 0;
#endif
/* current attributes - and out important bits */
  int total;      /* total for attron command */
  int offtotal;   /* total for attroff command */

/* NCURSES isn't smart enough to use the following code. eg. it
   doesn't handle terminals that toggle bold, standout or underline.
   So we'll have to check the values ourselves, and triple the
   length of this simple subroutine!!

  total=0;
  if (bold == 'y')  total |= A_BOLD;
  if (so == 'y')    total |= A_STANDOUT;
  if (under == 'y') total |= A_UNDERLINE;
  attroff( A_BOLD | A_UNDERLINE | A_STANDOUT );
  attron( total );
*/

  total=0;             /* clear the totals */
  offtotal=0;

  /* get current attributes */

#ifdef NCURSES3
    /* Old ncurses format */
    current=attr_get();
#else
#ifdef NCURSES5
    /* ncurses 5 will hang with attr_get: need wattr_get */
    wattr_get( stdscr, &current, &temp, NULL );
#else
    /* Normal ncurses */
    attr_get( &current, &temp, NULL ); 
#endif
#endif

  if (bold == 'y') {
     if (( current & A_BOLD )==0) {
        total |= A_BOLD;
     }
  }
  if (bold == 'n') {
     if (( current & A_BOLD )>=1) {
        offtotal |= A_BOLD;
     }
  }
  if (so == 'y') {
     if (( current & A_STANDOUT )==0) {
        total |= A_STANDOUT;
     }
  }
  if (so == 'n') {
     if (( current & A_STANDOUT )>=1) {
        offtotal |= A_STANDOUT;
     }
  }
  if (under == 'y') {
     if (( current && A_UNDERLINE )==0) {
        total |= A_UNDERLINE;
     }
  }
  if (under == 'n') {
     if (( current & A_UNDERLINE )>=1) {
        offtotal |= A_UNDERLINE;
     }
  }
  attroff( offtotal ); /* turn off anything that needs to be off */
  attron( total );     /* turn on active attributes */
}

void DrawChar( ch )
  char ch;
{
  echochar( (chtype)ch ); /* echo character and redraw */
}

char CGetKey () /* Get a Keypress or wait for one */
{
  chtype c;                 /* to hold the keypress */

  cbreak();
                            /* no keyboard buffering; nonl() implied */
  noecho();                 /* don't echo to the screen */
  intrflush(stdscr, FALSE); /* no fancy keyboard flushing */
  keypad(stdscr, TRUE);     /* return special function keys */

  #ifdef GPM
  if (C_hasmouse) {
     c = Gpm_Getch();
  } else {
     c = getch();              /* get a keypress */
  }
  #else
     c = getch();              /* get a keypress */
  #endif

  /* Can't recover this way */
  /* echo();                */   /* in case of problems */
  /* nocbreak();            */
  /* nonl();                */

  /* translate special keys into 7-bit ASCII chars */

  if ( KEY_BACKSPACE != KEY_LEFT ) { /* we've gotta be able to move left! */
     if ( (char)c == (char)8 )       /* then equate ASCII backspace */
        c = (chtype) 127;            /* with delete! */
     if ( c == KEY_BACKSPACE ) c = (chtype) 127; /* ditto */
  }
  /* --- ncurses mouse support -- */
  if ( c == KEY_MOUSE ) {                         /* mouse event? */
     if ( getmouse( &mousedata )==OK ) {          /* then get the info */
        C_mousex = mousedata.x;                   /* save mouse location */
        C_mousey = mousedata.y;
       if ( mousedata.bstate == BUTTON2_CLICKED ) /* treat buttons 2,3,4 */
          c = (chtype) 27;                        /* as ESC key for */
       if ( mousedata.bstate == BUTTON3_CLICKED ) /* accessories menu */
          c = (chtype) 27; 
       if ( mousedata.bstate == BUTTON4_CLICKED )
          c = (chtype) 27;
       if ( mousedata.bstate == REPORT_MOUSE_POSITION ) /* movement */
          c = (chtype) 254;                       /* character 254 */
       if ( mousedata.bstate == BUTTON1_CLICKED ) /* note button 1 as */
          c = (chtype) 255;                       /* character 255 */
    } 
  }
  /* --- end of mouse handling --- */
  if ( c == KEY_UP )       c = (chtype) 11; /* up arrow = vertical tab */
  if ( c == KEY_DOWN )     c = (chtype) 10; /* down arrow = line feed */
  if ( c == KEY_LEFT )     c = (chtype)  8; /* left arrow = backspace */
  if ( c == KEY_RIGHT )    c = (chtype) 21; /* right arrow  = forespace */
  if ( c == KEY_HOME )     c = (chtype) 25; /* home key = ctrl-y */
  if ( c == KEY_BEG )      c = (chtype) 25; /* beginning key = "home" */
  if ( c == KEY_PPAGE )    c = (chtype) 16; /* page up = ctrl-p */
  if ( c == KEY_NPAGE )    c = (chtype) 14; /* page down = ctrl-n */
  if ( c == KEY_END )      c = (chtype)  5; /* end key = ctrl-e 2 */
  if ( c == KEY_HELP )     c = (chtype) 27; /* help = ESC key */
  if ( c == KEY_OPTIONS )  c = (chtype) 27; /* options = "help" (for now) */
  if ( c == KEY_BTAB )     c = (chtype) 20; /* back tab = ctrl-t */
  if ( c == KEY_NEXT )     c = (chtype)  9; /* next object = tab */
  if ( c == KEY_PREVIOUS ) c = (chtype) 20; /* previous object = backtab */
  if ( c == KEY_COPY )     c = (chtype)  2; /* copy = ctrl-b (for now) */
  if ( c == KEY_REPLACE )  c = (chtype) 22; /* paste = ctrl-v */

  return (char) c;
}

char CKeyDelay () /* Get a Keypress, or return null if none */
                         /* Wait up to 1/10th second. */
{
  chtype c;                 /* to hold the keypress */

  halfdelay(1);             /* wait 1/10th sec before giving up */
  noecho();                 /* don't echo to the screen */
  nonl();                   /* don't translate return into newline */
  intrflush(stdscr, FALSE); /* no fancy keyboard flushing */
  keypad(stdscr, TRUE);     /* return special function keys */

  #ifdef GPM
  if (C_hasmouse) {
     c = Gpm_Getch();
  } else {
     c = getch();              /* get a keypress */
  }
  #else
     c = getch();              /* get a keypress */
  #endif

  /* Can't recover this way */
  /* echo();                */   /* in case of problems */
  /* nocbreak();            */
  /* nonl();                */

  /* translate special keys into ASCII chars */

  if ( c == (chtype)ERR )          c = (chtype)    0; /* no key = ASCII null */
  if ( (char)c == (char)8 ) c = (chtype) 127; /* backspace = delete! */
  if ( c == KEY_BACKSPACE ) c = (chtype) 127; /* ditto */
  /* --- ncurses mouse support -- */
  if ( c == KEY_MOUSE ) {                         /* mouse event? */
     if ( getmouse( &mousedata )==OK ) {          /* then get the info */
        C_mousex = mousedata.x;                   /* save mouse location */
        C_mousey = mousedata.y;
       if ( mousedata.bstate == BUTTON2_CLICKED ) /* treat buttons 2,3,4 */
          c = (chtype) 27;                        /* as ESC key for */
       if ( mousedata.bstate == BUTTON3_CLICKED ) /* accessories menu */
          c = (chtype) 27; 
       if ( mousedata.bstate == BUTTON4_CLICKED )
          c = (chtype) 27;
       if ( mousedata.bstate == BUTTON1_CLICKED ) /* note button 1 as */
          c = (chtype) 255;                       /* character 255 */
    } 
  }
  /* --- end of mouse handling --- */
  if ( c == KEY_UP )       c = (chtype) 11; /* up arrow = vertical tab */
  if ( c == KEY_DOWN )     c = (chtype) 10; /* down arrow = line feed */
  if ( c == KEY_LEFT )     c = (chtype)  8; /* left arrow = backspace */
  if ( c == KEY_RIGHT )    c = (chtype) 21; /* right arrow  = forespace */
  if ( c == KEY_HOME )     c = (chtype) 25; /* home key = ctrl-y */
  if ( c == KEY_BEG )      c = (chtype) 25; /* beginning key = "home" */
  if ( c == KEY_PPAGE )    c = (chtype) 16; /* page up = ctrl-p */
  if ( c == KEY_NPAGE )    c = (chtype) 14; /* page down = ctrl-n */
  if ( c == KEY_END )      c = (chtype)  5; /* end key = ctrl-e 2 */
  if ( c == KEY_HELP )     c = (chtype) 27; /* help = ESC key */
  if ( c == KEY_OPTIONS )  c = (chtype) 27; /* options = "help" (for now) */
  if ( c == KEY_BTAB )     c = (chtype) 20; /* back tab = ctrl-t */
  if ( c == KEY_NEXT )     c = (chtype)  9; /* next object = tab */
  if ( c == KEY_PREVIOUS ) c = (chtype) 20; /* previous object = backtab */
  if ( c == KEY_COPY )     c = (chtype)  2; /* copy = ctrl-b (for now) */
  if ( c == KEY_REPLACE )  c = (chtype) 22; /* paste = ctrl-v */

  return (char) c;
}

char CKeypress () /* Get a Keypress, or return null if none */
{
  chtype c;                 /* to hold the keypress */

  cbreak();
  nodelay(stdscr,TRUE);     /* don't wait before giving up */
  noecho();                 /* don't echo to the screen */
  nonl();                   /* don't translate return into newline */
  intrflush(stdscr, FALSE); /* no fancy keyboard flushing */
  keypad(stdscr, TRUE);     /* return special function keys */

  #ifdef GPM
  if (C_hasmouse) {
     c = Gpm_Getch();
  } else {
     c = getch();              /* get a keypress */
  }
  #else
     c = getch();              /* get a keypress */
  #endif

  /* Can't recover via nocbreak mode */
  /* echo();                */   /* in case of problems */
  nodelay(stdscr,FALSE);    /* back to normal */
  /* nocbreak(); */
  /* nonl(); */

  /* translate special keys into ASCII chars */

  if ( c == (chtype)ERR )          c = (chtype)  0; /* no key = ASCII null */
  if ( (char)c == (char)8 ) c = (chtype) 127; /* backspace = delete! */
  if ( c == KEY_BACKSPACE ) c = (chtype) 127; /* ditto */
  /* --- ncurses mouse support -- */
  if ( c == KEY_MOUSE ) {                         /* mouse event? */
     if ( getmouse( &mousedata )==OK ) {          /* then get the info */
        C_mousex = mousedata.x;                   /* save mouse location */
        C_mousey = mousedata.y;
       if ( mousedata.bstate == BUTTON2_CLICKED ) /* treat buttons 2,3,4 */
          c = (chtype) 27;                        /* as ESC key for */
       if ( mousedata.bstate == BUTTON3_CLICKED ) /* accessories menu */
          c = (chtype) 27; 
       if ( mousedata.bstate == BUTTON4_CLICKED )
          c = (chtype) 27;
       if ( mousedata.bstate == BUTTON1_CLICKED ) /* note button 1 as */
          c = (chtype) 255;                       /* character 255 */
    } 
  }
  /* --- end of mouse handling --- */
  if ( c == KEY_UP )       c = (chtype) 11; /* up arrow = vertical tab */
  if ( c == KEY_DOWN )     c = (chtype) 10; /* down arrow = line feed */
  if ( c == KEY_LEFT )     c = (chtype)  8; /* left arrow = backspace */
  if ( c == KEY_RIGHT )    c = (chtype) 21; /* right arrow  = forespace */
  if ( c == KEY_HOME )     c = (chtype) 25; /* home key = ctrl-y */
  if ( c == KEY_BEG )      c = (chtype) 25; /* beginning key = "home" */
  if ( c == KEY_PPAGE )    c = (chtype) 16; /* page up = ctrl-p */
  if ( c == KEY_NPAGE )    c = (chtype) 14; /* page down = ctrl-n */
  if ( c == KEY_END )      c = (chtype)  5; /* end key = ctrl-e 2 */
  if ( c == KEY_HELP )     c = (chtype) 27; /* help = ESC key */
  if ( c == KEY_OPTIONS )  c = (chtype) 27; /* options = "help" (for now) */
  if ( c == KEY_BTAB )     c = (chtype) 20; /* back tab = ctrl-t */
  if ( c == KEY_NEXT )     c = (chtype)  9; /* next object = tab */
  if ( c == KEY_PREVIOUS ) c = (chtype) 20; /* previous object = backtab */
  if ( c == KEY_COPY )     c = (chtype)  2; /* copy = ctrl-b (for now) */
  if ( c == KEY_REPLACE )  c = (chtype) 22; /* paste = ctrl-v */

  return (char) c;
}

void FlushKeys()
{
  flushinp();
}

void CSpoolRect( left, top, right, bottom, ch )
  int left, top, right, bottom;
  char ch;
{
  int  x, y;

  for( x=0; x<=right-left; x++) sline[x] = ch;
  sline[right-left+1] = '\0';

  for ( y=top; y<=bottom; y++) {
    mvaddstr( y, left, sline );
  }
}

void CANSIColour ( int colour ) {
if (has_colors() ) {

  if (colour<0)   attron( COLOR_PAIR( 8 ) );
  if (colour==0)  attron( COLOR_PAIR( 1 ) );
  if (colour==1)  attron( COLOR_PAIR( 2 ) );
  if (colour==2)  attron( COLOR_PAIR( 3 ) );
  if (colour==3)  attron( COLOR_PAIR( 4 )  ); /* yellow */
  if (colour==4)  attron( COLOR_PAIR( 5 )  );
  if (colour==5)  attron( COLOR_PAIR( 6 )  );
  if (colour==6)  attron( COLOR_PAIR( 7 ) );
  if (colour==7)  attron( COLOR_PAIR( 8 )  );
  if (colour==8)  attron( COLOR_PAIR( 9 )  );
  if (colour==9)  attron( COLOR_PAIR( 10 )  );
  if (colour==10) attron( COLOR_PAIR( 11 )  );
  if (colour==11) attron( COLOR_PAIR( 12 )  ); /* therm back */
  if (colour==12) attron( COLOR_PAIR( 13 )  ); /* scroll bar back */
  if (colour>12)  attron( COLOR_PAIR( 7 )  );
}

/*
  if (colour==100) printf( "40m" );
  if (colour==101) printf( "41m" );
  if (colour==102) printf( "42m" );
  if (colour==103) printf( "43m" );
  if (colour==104) printf( "44m" );
  if (colour==105) printf( "45m" );
  if (colour==106) printf( "46m" );
  if (colour==107) printf( "47m" );
*/
  /* 
  if (colour==-1) printf( "\033[0;37;40m" );
  printf( "\033[" );
  if (colour<100) printf( "40;" );
  if (colour==0) printf( "30m" );
  if (colour==1) printf( "31m" );
  if (colour==2) printf( "32m" );
  if (colour==3) printf( "33m" );
  if (colour==4) printf( "34m" );
  if (colour==5) printf( "35m" );
  if (colour==6) printf( "36m" );
  if (colour==7) printf( "37m" );
  if (colour>=100) printf( "37;" );
  if (colour==100) printf( "40m" );
  if (colour==101) printf( "41m" );
  if (colour==102) printf( "42m" );
  if (colour==103) printf( "43m" );
  if (colour==104) printf( "44m" );
  if (colour==105) printf( "45m" );
  if (colour==106) printf( "46m" );
  if (colour==107) printf( "47m" );
  */
}

void Cls()
{
  move( 0, 0 );
  clrtobot();
  refresh();
  if (colour_flag) { 
     CANSIColour( 0 );
     CSpoolRect( 0, 0, cols-1, lines-1, ' ' );
  }
}

void Refresh ()
{
  refresh();                 /* redraw the screen */
}

void ShutdownCurses ()
{
  #ifdef GPM
  Gpm_Close();
  C_hasmouse = 0;
  #endif
  endwin();                 /* shutdown curses */
}

void SpoolChar( char ch )
{
  if ( ch == (char) 9 ) { /* change tabs to spaces */
      ch=' ';
  }
  if ( ch < ' ' ) {       /* change control chars to ? */
     ch='?';
  }
  addch( (chtype) ch ); /* character will be drawn at next refresh */
}

void CDesktop( int maxx, int maxy ) {
 int x, y;

 if (has_colors()) attron( COLOR_PAIR( 14 ) );
 for (x=0; x<=maxx; x++) {
     chline[x]=ACS_CKBOARD;
 }
 for (y=0; y<=maxy; y++) mvaddchstr( y, 0, chline );
}

void CBeep() {
  beep(); /* beep terminal */
}

void SpoolSpecial( val )
  int val;
{
  if (val==0) addch( ACS_ULCORNER );
  if (val==1) addch( ACS_LLCORNER );
  if (val==2) addch( ACS_URCORNER );
  if (val==3) addch( ACS_LRCORNER );
  if (val==4) addch( ACS_HLINE );
  if (val==5) addch( ACS_VLINE );
}

void ResetCurses() {
  /* redrawwin( stdscr ); doesnt seem to work */
  /* endwin();
  initscr(); */
  /* if the window has been resized, this can cause problems */
  clearok( curscr, TRUE );
}

void SetColour( int cmode ) {
  /* set background colour to blue or black */
  colour_flag = 0;
  if (has_colors()) {
     colour_flag = 1;
     start_color();  /* start colour support */
     if (cmode==0) { /* blue background */
        init_pair(  1, COLOR_BLACK, COLOR_BLUE );
        init_pair(  2, COLOR_RED, COLOR_BLUE );
        init_pair(  3, COLOR_GREEN, COLOR_BLUE );
        init_pair(  4, COLOR_YELLOW, COLOR_BLUE );
        init_pair(  5, COLOR_BLUE, COLOR_BLUE );
        init_pair(  6, COLOR_MAGENTA, COLOR_BLUE );
        init_pair(  7, COLOR_CYAN, COLOR_BLUE );
        init_pair(  8, COLOR_WHITE, COLOR_BLUE );
        init_pair(  9, COLOR_WHITE, COLOR_BLACK );    /* input */
        init_pair( 10, COLOR_YELLOW, COLOR_RED );     /* thermometer */
        init_pair( 11, COLOR_YELLOW, COLOR_RED );     /* scroll bar */
        init_pair( 12, COLOR_RED, COLOR_YELLOW );     /* thermometer back */
        init_pair( 13, COLOR_RED, COLOR_WHITE );      /* scroll bar back */
        init_pair( 14, COLOR_BLUE, COLOR_BLACK );     /* desktop back */
     } else { /* black background */
        start_color();  /* start colour support */
        init_pair(  1, COLOR_BLACK, COLOR_BLACK );
        init_pair(  2, COLOR_RED, COLOR_BLACK );
        init_pair(  3, COLOR_GREEN, COLOR_BLACK );
        init_pair(  4, COLOR_YELLOW, COLOR_BLACK );
        init_pair(  5, COLOR_BLUE, COLOR_BLACK );
        init_pair(  6, COLOR_MAGENTA, COLOR_BLACK );
        init_pair(  7, COLOR_CYAN, COLOR_BLACK );
        init_pair(  8, COLOR_WHITE, COLOR_BLACK );
        init_pair(  9, COLOR_WHITE, COLOR_BLUE );     /* input */
        init_pair( 10, COLOR_YELLOW, COLOR_RED );     /* thermometer */
        init_pair( 11, COLOR_YELLOW, COLOR_RED );     /* scroll bar */
        init_pair( 12, COLOR_RED, COLOR_YELLOW );     /* thermometer back */
        init_pair( 13, COLOR_RED, COLOR_WHITE );      /* scroll bar back */
        init_pair( 14, COLOR_BLUE, COLOR_BLACK );     /* desktop back */
     }
  }
}

void StartupCurses()
{
  extern int colour_flag;
  extern int  lines;
  extern int  cols;

  initscr();                        /* startup curses */
  if ( strcmp( NCURSES_VERSION, "4.0" ) < 0 ) {
     printf( "This versionr requires ncurses 4.0 or greater\n" );
  }
  SetColour( 0 );
  lines=LINES; /* number of lines */
  cols=COLS;   /* number of columns */

  /* Normally, Return counts as a NewLine (LF+CR) */
  /* If I leave things as normal, CR's when not waiting for input */
  /* become LF's!  I would like to leave things in normal curses' */
  /* mode in case of a crash, but I can't with this! */

  cbreak();   /* curses doesn't set mode at startup...we'll take cbreak */
              /* nonl() is irrelivant is cbreak mode */
  flushinp(); /* throw away any waiting characters */
  refresh();

  C_hasmouse = 0;
  /* --- ncurses mouse support --- */
  C_hasmouse = ( mousemask( REPORT_MOUSE_POSITION |
                            BUTTON1_CLICKED |
                            BUTTON2_CLICKED |
                            BUTTON3_CLICKED |
                            BUTTON4_CLICKED, 0  )==0 ) ? 0 : 1;
  /* --- end of ncurses mouse support */

  /* --- GPM mouse support not finished */

  #ifdef GPM
  conn.eventMask = ~0;
  conn.defaultMask = GMP_MOVE | GPM_HARD;
  conn.maxMod = ~0;
  conn.minMod = 0;
  if ( (mouse_fid = Gpm_Open(&conn, 0)) == -1) {
/*      attrset(COLOR_PAIR(TITLE_COLOR) | A_BOLD | A_ALTCHARSET);
        move(0, 35);
        addstr("mouse off");
*/
      refresh();
  } else {
    C_hasmouse = 1;
    gpm_handler = MouseHandler;
  }
  #endif
}


/* Mouse Handler -------------------------------------------------- */
/*                                                                  */
/* Set the global variables:                                        */
/* int C_mousebutton;  --1 = mouse button down, shared with Ada     */
/* int C_mousex;       --mouse x coordinate, shared with Ada        */
/* int C_mousey;       --mouse y coordinate, shared with Ada        */
/* ---------------------------------------------------------------- */

#ifdef GPM
int MouseHandler(Gpm_Event * event, void *data) {

    if (event->type & GPM_DOWN) {
       C_mousebutton = 1;
       C_mousex = event->x;
       C_mousey = event->y;
    } else if (event->type & GPM_UP) {
       C_mousebutton = 0;
       C_mousex = event->x;
       C_mousey = event->y;
    }
    return 0;
}
#endif                          /* end of GPM stuff */

