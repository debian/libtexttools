with Ada.Text_IO,
     Common,
     OS;

use  Ada.Text_IO,
     Common,
     OS;

procedure os_demo is
begin
  put_line( "This is a demonstration of the TextTools O/S Package" );
  new_line;

  StartupCommon( "os_demo", "os_demo" );
  StartupOS;

  ShutdownOS;
  ShutdownCommon;
end os_demo;

