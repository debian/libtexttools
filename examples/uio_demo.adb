--  with common, userio;
--  use  common, userio;

--  with text_io;
--  use text_io;

procedure uio_demo is
  --  ch : character;

  --  ir1 : anInputRecord( InputType => KeyInput );
  --  ir2 : anInputRecord( InputType => HeldKeyInput );
  --  ir3 : anInputRecord( InputType => DirectionInput );
  --  ir4 : anInputRecord( InputType => LocationInput );
  --  ir5 : anInputRecord( InputType => ButtonDownInput );
  --  ir6 : anInputRecord( InputType => ButtonUpInput );
  --  ir7 : anInputRecord( InputType => MoveInput );
  --  ir8 : anInputRecord( InputType => UserInput );

begin
   null;
   --  StartupCommon( "uio_demo", "User IO Demo" );
  --  StartupUserIO;
  --  Draw( "This is a string" );
  --  GetKey( ch );
  --  ShutdownUserIO;
  --  ShutdownCommon;

  --  put( "Input record is" );
  --  put( integer'image(ir1'size/8) );
  --  put_line( " bytes" );

  --  put( "Input record is" );
  --  put( integer'image(ir5'size/8) );
  --  put_line( " bytes" );

  --  put( "InputType is" );
  --  put( integer'image(ir1.InputType'size/8) );
  --  put_line( " bytes" );

  --  put( "TimeStamp is" );
  --  put( integer'image(ir1.TimeStamp'size/8) );
  --  put_line( " bytes" );

  --  put( "Key =" );
  --  put( integer'image(ir1.key'size/8) );
  --  put_line( " bytes" );

  --  put( "HeldKey =" );
  --  put( integer'image(ir2.heldkey'size/8) );
  --  put_line( " bytes" );

  --  put( "Direction =" );
  --  put( integer'image(ir3.direction'size/8) );
  --  put_line( " bytes" );

  --  put( "Velocity =" );
  --  put( integer'image(ir3.velocity'size/8) );
  --  put_line( " bytes" );

  --  put( "X =" );
  --  put( integer'image(ir4.x'size/8) );
  --  put_line( " bytes" );

  --  put( "Y =" );
  --  put( integer'image(ir4.y'size/8) );
  --  put_line( " bytes" );

  --  put( "DownButton =" );
  --  put( integer'image(ir5.downButton'size/8) );
  --  put_line( " bytes" );

  --  put( "DownLocationX =" );
  --  put( integer'image(ir5.downLocationX'size/8) );
  --  put_line( " bytes" );

  --  put( "DownLocationY =" );
  --  put( integer'image(ir5.downLocationY'size/8) );
  --  put_line( " bytes" );

  --  put( "UpButton =" );
  --  put( integer'image(ir6.upButton'size/8) );
  --  put_line( " bytes" );

  --  put( "UpLocationX =" );
  --  put( integer'image(ir6.upLocationX'size/8) );
  --  put_line( " bytes" );

  --  put( "UpLocationY =" );
  --  put( integer'image(ir6.upLocationY'size/8) );
  --  put_line( " bytes" );

  --  put( "MoveLocationX =" );
  --  put( integer'image(ir7.moveLocationX'size/8) );
  --  put_line( " bytes" );

  --  put( "MoveLocationY =" );
  --  put( integer'image(ir7.moveLocationY'size/8) );
  --  put_line( " bytes" );

  --  put( "ID =" );
  --  put( integer'image(ir8.id'size/8) );
  --  put_line( " bytes" );


end uio_demo;

