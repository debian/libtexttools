with Common, OS, UserIO, Controls, Windows;
use  Common, OS, UserIO, Controls, Windows;

procedure listinfo is
  line1 : aliased AStaticLine;
  line2 : aliased AStaticLine;
  ok    : aliased ASimpleButton;
  DT    : aDialogTaskRecord;

  list  : Strlist.Vector;
begin
  StartupCommon( "TIA", "tia" );
  StartupOS;
  StartupUserIO;
  StartupControls;
  StartupWindows;

  OpenWindow( "ShowListInfo Demo", 0, 0, 60, 20, normal, false );
  DrawWindow;

  Init( line1, 2, 2, 58, 2 );
  SetText( line1, "This a demonstration of ShowListInfo." );
  AddControl( line1'unchecked_access );

  Init( line2, 2, 3, 58, 3 );
  SetText( line2,  "It displays a list that the user can view." );
  AddControl( line2'unchecked_access );

  Init( ok, 2, 18, 30, 18, 'o' );
  SetText( ok, "OK" );
  AddControl( ok'unchecked_access );

  DoDialog( DT );
  pragma Unreferenced (DT);
  LoadList( "listinfo.adb", list );
  ShowListInfo( "Source code for this program", 0, 1, 79, 24, list );
  pragma Unreferenced (List);
  CloseWindow;

  ShutdownWindows;
  ShutdownControls;
  ShutdownUserIO;
  ShutdownOS;
  ShutdownCommon;

end listinfo;

