with Common, OS, UserIO, Controls, Windows;
use  Common, OS, UserIO, Controls, Windows;

procedure alert is
  line1 : aliased AStaticLine;
  line2 : aliased AStaticLine;
  ok    : aliased ASimpleButton;
  DT    : aDialogTaskRecord;

  b     : boolean;
  pragma Unreferenced (B);
  id    : AControlNumber;
  pragma Unreferenced (Id);
begin
  StartupCommon( "TIA", "tia" );
  StartupOS;
  StartupUserIO;
  StartupControls;
  StartupWindows;

  OpenWindow( "Alert Demo", 0, 0, 40, 20, normal, false );
  DrawWindow;

  Init( line1, 2, 2, 38, 2 );
  SetText( line1, "This a demonstration of alerts." );
  AddControl( line1'unchecked_access );

  Init( line2, 2, 3, 38, 3 );
  SetText( line2,  "There are 7 types of alert windows." );
  AddControl( line2'unchecked_access );

  Init( ok, 2, 18, 30, 18, 'o' );
  SetText( ok, "OK" );
  AddControl( ok'unchecked_access );

  DoDialog( DT );
  pragma Unreferenced (DT);
  CloseWindow;

  NoteAlert( "This is a note alert" );
  CautionAlert( "This is a caution alert" );
  StopAlert( "This is a stop alert" );
  b := YesAlert( "This is a yes alert", warning );
  b := YesAlert( "This is a no alert", warning );
  b := CancelAlert( "This is a cancel alert", "Do it", warning );
  id := YesCancelAlert( "This is a yes/cancel alert", warning );

  ShutdownWindows;
  ShutdownControls;
  ShutdownUserIO;
  ShutdownOS;
  ShutdownCommon;

end alert;

