--    Project to test Texttools.
--    Copyright (C) 2010-2012 Nicolas Boulenguez <nicolas.boulenguez@free.fr>
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.

with "texttools";
project Examples is
  for Main use ("alert.adb", "basic2.adb", "basic.adb", "listinfo2.adb",
                "listinfo.adb", "os_demo.adb", "scrollable.adb", "try_unix.adb",
                "uio2_demo.adb", "uio_demo.adb");
  for Source_Dirs use (".");
  for Object_Dir use ".";
  for Exec_Dir use ".";
  for Languages use ("Ada");
  package Compiler is
     for Default_Switches ("Ada") use ("ADAFLAGS");
  end Compiler;
  package Linker is
     for Default_Switches ("Ada") use ("LDFLAGS");
  end Linker;
end Examples;
