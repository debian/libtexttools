with Os;

procedure Try_Unix is

   -- CGetUNIX
   S1 : constant String := Os.UNIX ("echo hello");
   pragma Assert (S1 = "hello");

begin
   null;
end Try_Unix;
