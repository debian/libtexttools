with Common, OS, UserIO, Controls, Windows;
use  Common, OS, UserIO, Controls, Windows;

procedure basic is
   ch : character; -- key pressed
   pragma Unreferenced (Ch);
begin
  StartupCommon( "TIA", "tia" );
  StartupOS;
  StartupUserIO;
  StartupControls;
  StartupWindows;

  OpenWindow( "Basic Window", 0, 0, 40, 20, normal, false );
  DrawWindow;
  MoveTo( 2, 2 );
  Draw( "This is a basic TextTools window." );
  MoveTo( 2, 3 );
  Draw( "It contains no controls." );
  MoveTo( 2, 18 );
  Draw( "Press any key to quit." );

  GetKey( ch );

  CloseWindow;

  ShutdownWindows;
  ShutdownControls;
  ShutdownUserIO;
  ShutdownOS;
  ShutdownCommon;

end basic;

