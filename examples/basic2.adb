with Common, OS, UserIO, Controls, Windows;
use  Common, OS, UserIO, Controls, Windows;

procedure basic2 is
  line1 : aliased AStaticLine;
  line2 : aliased AStaticLine;
  ok    : aliased ASimpleButton;
  DT    : aDialogTaskRecord;
begin
  StartupCommon( "TIA", "tia" );
  StartupOS;
  StartupUserIO;
  StartupControls;
  StartupWindows;

  OpenWindow( "Basic Window 2", 0, 0, 40, 20, normal, false );
  DrawWindow;

  Init( line1, 2, 2, 38, 2 );
  SetText( line1, "This is a basic TextTools window." );
  AddControl( line1'unchecked_access );

  Init( line2, 2, 3, 38, 3 );
  SetText( line2,  "It contains controls." );
  AddControl( line2'unchecked_access );

  Init( ok, 2, 18, 30, 18, 'o' );
  SetText( ok, "OK" );
  AddControl( ok'unchecked_access );

  DoDialog( DT );
  pragma Unreferenced (DT);
  CloseWindow;

  ShutdownWindows;
  ShutdownControls;
  ShutdownUserIO;
  ShutdownOS;
  ShutdownCommon;

end basic2;

