with common, userio;
use  common, userio;

procedure uio2_demo is
   ch : character;
   pragma Unreferenced (Ch);
  r  : aRect;
begin
  StartupCommon( "uio2_demo", "User IO Demo 2" );
  StartupUserIO;

  SetPenColour( white );

  Draw( "There should be a brief pause before the rectangle appears" );
  WaitToReveal;
  SetRect( r, 10, 10, 20 , 20 );
  MoveToGlobal( 0, 9 );
  Draw( "FrameRect" );
  FrameRect( r );
  WaitFor( 200 );
  Reveal;

  MoveToGlobal( 0, 23 );
  Draw( "Press a key" );
  GetKey( ch );
  ShutdownUserIO;
  ShutdownCommon;

end uio2_demo;

